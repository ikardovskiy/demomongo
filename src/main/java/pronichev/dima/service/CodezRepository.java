package pronichev.dima.service;


import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import reactor.core.publisher.Flux;

public interface CodezRepository extends ReactiveSortingRepository<CountryCodez, String> {
    Flux<CountryCodez> findByCountryContains(String pattern);
}
