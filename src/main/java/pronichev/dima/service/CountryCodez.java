package pronichev.dima.service;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(value = "codez")
public class CountryCodez {

    @Id
    private String name;
    private String country;
    private String code;

    public CountryCodez(){

    }

    public CountryCodez(String name, String country, String code) {
        this.name = name;
        this.country = country;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
