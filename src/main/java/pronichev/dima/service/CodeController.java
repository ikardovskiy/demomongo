package pronichev.dima.service;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.*;

@RestController
public class CodeController {

    @Autowired
    WebClient.Builder webClientBuilder;

    @Autowired
    CodezRepository repository;

    @GetMapping("/rest/code")
    public Mono<ResponseEntity<List<CountryCodez>>> getCountryCodez(@RequestParam String country) {
        return repository.findByCountryContains(country).collectList()
                .map(cc -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                        .body(cc));
    }

    @GetMapping("/rest/code2")
    public Mono<ResponseEntity<List<CountryCodez>>> getCountryCodez2(@RequestParam String country) {
        return Mono.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                .body(Arrays.asList(new CountryCodez("BE", "Belgium", "32"))));
    }

    @GetMapping("/rest/loaddata")
    public Mono<Void> loadData() {

        return repository.deleteAll()
                .thenMany(Flux.just(new CountryCodez("BE", "Belgium", "32"),
                        new CountryCodez("BY", "Belarus", "375")
                ))
                .flatMap(c -> repository.save(c)).then();
    }

    @GetMapping("/rest/loaddata2")
    public Mono<Void> loadData2() {
        return webClientBuilder.baseUrl("http://country.io/").build()
                .get()
                .uri("names.json")
                .exchange()
                .flatMap(response -> response.bodyToMono(JsonNode.class))
                .doOnNext(n -> System.out.println(n))
                .map(n -> {
                    Map<String, String> map = new HashMap<>();
                    final Iterator<String> fieldz = n.fieldNames();
                    while (fieldz.hasNext()) {
                        String fieldName = fieldz.next();
                        map.put(fieldName, n.get(fieldName).textValue());
                    }
                    return map;
                })
                .flatMap(names ->
                        webClientBuilder.baseUrl("http://country.io/").build()
                                .get()
                                .uri("phone.json")
                                .exchange()
                                .flatMap(response -> response.bodyToMono(JsonNode.class))
                                .map(n -> {
                                    Map<String, String> map = new HashMap<>();
                                    final Iterator<String> fieldz = n.fieldNames();
                                    while (fieldz.hasNext()) {
                                        String fieldName = fieldz.next();
                                        map.put(fieldName, n.get(fieldName).textValue());
                                    }
                                    return map;
                                })
                                .map(phones -> {
                                    List<CountryCodez> codez = new ArrayList<>();
                                    for (String name : names.keySet()) {
                                        CountryCodez code = new CountryCodez();
                                        code.setName(name);
                                        code.setCountry(names.get(name));
                                        code.setCode(phones.get(name));
                                        codez.add(code);
                                    }
                                    return codez;
                                })

                )
                .flatMap(codez -> repository.deleteAll().thenMany(repository.saveAll(codez)).then());
    }


}
