FROM openjdk:8
RUN mkdir /usr/src/service
COPY target/service-0.0.1-SNAPSHOT.jar /usr/src/service
EXPOSE 8080
WORKDIR /usr/src/service
CMD ["java", "-jar", "service-0.0.1-SNAPSHOT.jar"]